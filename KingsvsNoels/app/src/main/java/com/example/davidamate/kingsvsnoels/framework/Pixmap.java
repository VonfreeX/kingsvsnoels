package com.example.davidamate.kingsvsnoels.framework;

import com.example.davidamate.kingsvsnoels.framework.Graphics.PixmapFormat;

public interface Pixmap {
    public int getWidth();

    public int getHeight();

    public PixmapFormat getFormat();

    public void dispose();
}
