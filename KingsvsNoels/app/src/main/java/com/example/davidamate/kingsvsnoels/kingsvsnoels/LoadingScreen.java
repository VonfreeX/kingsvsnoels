package com.example.davidamate.kingsvsnoels.kingsvsnoels;

import com.example.davidamate.kingsvsnoels.framework.Game;
import com.example.davidamate.kingsvsnoels.framework.Graphics;
import com.example.davidamate.kingsvsnoels.framework.Screen;
import com.example.davidamate.kingsvsnoels.framework.Graphics.PixmapFormat;

/**
 * Created by david.amate on 21/12/2016.
 */

    public class LoadingScreen extends Screen {

        public LoadingScreen(Game game) {
            super(game);

        }

        public void update(float deltaTime) {
            Graphics g = game.getGraphics();
            Assets.background = g.newPixmap("background.jpg", PixmapFormat.RGB565);
            //Assets.King1 = g.newPixmap("King1.png", PixmapFormat.RGB565);
            //Assets.King2 = g.newPixmap("King2.png", PixmapFormat.RGB565);
            //Assets.King3 = g.newPixmap("King3.png", PixmapFormat.RGB565);
            Assets.buttons=g.newPixmap("buttons.png", PixmapFormat.RGB565);
            Assets.MenuPause=g.newPixmap("pausemenu.png", PixmapFormat.RGB565);
            Assets.Noel = g.newPixmap("Noel.png", PixmapFormat.RGB565);
            Assets.King1 = g.newPixmap("King1.png", PixmapFormat.RGB565);
            Assets.King2 = g.newPixmap("King2.png", PixmapFormat.RGB565);
            Assets.King3 = g.newPixmap("King3.png", PixmapFormat.RGB565);
            Assets.Tree = g.newPixmap("Tree.png", PixmapFormat.RGB565);
            Assets.GameTitle = g.newPixmap("GameTitle.png", PixmapFormat.RGB565);
            Assets.TrollNoel = g.newPixmap("TrollNoel.png", PixmapFormat.RGB565);
            Assets.Noel_girat = g.newPixmap("Noel_girat.png", PixmapFormat.RGB565);
            Assets.Play = g.newPixmap("Play.png", PixmapFormat.RGB565);
            Assets.ready = g.newPixmap("ready.png", PixmapFormat.RGB565);
            Assets.gameover = g.newPixmap("gameover.png", PixmapFormat.RGB565);
            Assets.exit = g.newPixmap("exit.png", PixmapFormat.RGB565);
            Assets.win1 = g.newPixmap("win1.png", PixmapFormat.RGB565);
            Assets.win2 = g.newPixmap("win2.png", PixmapFormat.RGB565);
            Assets.win3 = g.newPixmap("win3.png", PixmapFormat.RGB565);
            Assets.RETRY = g.newPixmap("RETRY.png", PixmapFormat.RGB565);
            Assets.intro=game.getAudio().newMusic("intro.mp3");
            Assets.gamemusic=game.getAudio().newMusic("ingame.mp3");
            Assets.gameOver=game.getAudio().newMusic("gameOver.mp3");
            Assets.Victory=game.getAudio().newMusic("Victory.mp3");
            game.setScreen(new MainMenuScreen(game));
            Assets.intro.setLooping(true);
            Assets.gamemusic.setLooping(true);

        }

        public void render(float deltaTime) {

        }

        public void pause() {

        }

        public void resume() {

        }

        public void dispose() {

        }
    }


