package com.example.davidamate.kingsvsnoels.kingsvsnoels;

import com.example.davidamate.kingsvsnoels.framework.Music;
import com.example.davidamate.kingsvsnoels.framework.Pixmap;
import com.example.davidamate.kingsvsnoels.framework.Sound;

/**
 * Created by david.amate on 21/12/2016.
 */
public class Assets {
    public static Pixmap background;
    public static Pixmap Play;
    public static Pixmap King1;
    public static Pixmap buttons;
    public static Pixmap MenuPause;
    public static Pixmap ready;
    public static Pixmap exit;
    public static Pixmap gameover;
    public static Pixmap King2;
    public static Pixmap King3;
    public static Pixmap Noel;
    public static Pixmap win1;
    public static Pixmap win2;
    public static Pixmap win3;
    public static Pixmap Noel_girat;
    public static Pixmap GameTitle;
    public static Pixmap TrollNoel;
    public static Pixmap Tree;
    public static Pixmap RETRY;
    public static Music intro;
    public static Music gamemusic;
    public static Music gameOver;
    public static Music Victory;

}
