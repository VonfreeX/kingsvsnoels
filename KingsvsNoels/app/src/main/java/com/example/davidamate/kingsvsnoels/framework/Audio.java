package com.example.davidamate.kingsvsnoels.framework;

public interface Audio {
    public Music newMusic(String filename);

    public Sound newSound(String filename);
}
