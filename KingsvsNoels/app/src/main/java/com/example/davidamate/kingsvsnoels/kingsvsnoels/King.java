package com.example.davidamate.kingsvsnoels.kingsvsnoels;

/**
 * Created by david.amate on 21/12/2016.
 */
public class King {
    public int y,x;
    public int direction;
    public int type;
    boolean xoc=false;
    boolean die=false;
    boolean selected=false;
    public static final int LEFT = 1;
    public static final int RIGHT = 2;
    public static final int UP = 3;
    public static final int DOWN = 4;
    boolean Victory=false;
    public King(int x,int type )
    {
        this.x=x;
        this.y=14;
        this.direction=UP;
        this.type=type;

    }
    public void setDirection(int direction)
    {
        this.direction=direction;
    }
    public void Movement()
    {
        if(selected) {
            if (direction == LEFT)
                this.x -= 1;

            if (direction == RIGHT)
                this.x += 1;
            if (direction == UP)
                this.y -= 1;

            if (direction == DOWN)
                this.y += 1;
            if (this.x <= -1) {
                this.x = 9;
            }
            if (this.x >= 10) {
                this.x = 0;
            }
            if (this.y >= 15) {
                this.y = 14;
            }
            if (this.y == 0) {
                Victory = true;
            }
            if (this.y <= 0) {
                y=0;
            }
        }
    }
    public boolean checkCollision(Noel[] noel)
    {
        for(int i=0;i<13;i++)
        {
            int x=noel[i].x;
            int y=noel[i].y;
            if(this.x==x&&this.y==y)
            {
                xoc= true;
            }

        }
        return xoc;
    }
}
//Para hacer el movimento comprobat TouchEvent.TOUCH_DOWN Y UP  Math.round()