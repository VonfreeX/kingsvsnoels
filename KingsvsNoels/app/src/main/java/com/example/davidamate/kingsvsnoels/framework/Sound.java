package com.example.davidamate.kingsvsnoels.framework;

public interface Sound {
    public void play(float volume);

    public void dispose();
}
