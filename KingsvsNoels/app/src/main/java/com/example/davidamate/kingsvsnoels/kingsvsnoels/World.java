package com.example.davidamate.kingsvsnoels.kingsvsnoels;

import java.util.Random;

/**
 * Created by david.amate on 21/12/2016.
 */
public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.55f;
    public Noel[] noel;
    public King[] Kings;
    int cont=0;
    int die=0;
    int speed_inc=0;
    Random random=new Random();
    public boolean gameOver = false;;
    public boolean Win = false;;
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        Kings=new King[3];
        noel=new Noel[13];
        for(int i=0;i<13;i++) {
            int x=random.nextInt(2)+1;
            if(x==2) {
                int z=random.nextInt(4);
                int speed=random.nextInt(2)+1;
                noel[i] = new Noel(z, i + 1, x,speed);
            }
            if(x==1) {
                int z=random.nextInt(4)+5;
                int speed=random.nextInt(3)+1;
                noel[i] = new Noel(z, i + 1, x,speed);
            }
        }
        Kings[0]=new King(5,1);
        Kings[0].selected=true;
        Kings[1]=new King(3,2);
        Kings[2]=new King(7,3);

    }

    public void update(float deltaTime) {
        if (gameOver) return;
        if (Win) return;
        tickTime += deltaTime;
        while (tickTime > tick) {
            if(speed_inc==0) {
                tick = 0.4F;
            }
            if(speed_inc==1) {
                tick = 0.3F;
            }
            if(speed_inc==2) {
                tick = 0.2F;
            }
            tickTime -= tick;
            if(cont <3) {
                if (Kings[cont].checkCollision(noel)) {
                    Kings[cont].selected = false;
                    Kings[cont].die = true;
                    die++;
                    cont++;
                    if (cont < 3) {
                        Kings[cont].selected = true;
                    }
                }
            }
            if(die==3)
            {
                gameOver=true;
            }
            else if (cont==3)
            {
                Win=true;
            }
             if(cont <3) {
                if(Kings[cont].Victory)
                {
                    Kings[cont].selected=false;
                    cont++;
                    speed_inc++;
                    if(cont <3) {
                        Kings[cont].selected = true;
                    }
                }
            }
            for (int i=0;i<13;i++)
            {
                noel[i].Movement();
            }
        }
    }

}
