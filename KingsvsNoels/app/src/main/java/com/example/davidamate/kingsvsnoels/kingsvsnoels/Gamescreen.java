package com.example.davidamate.kingsvsnoels.kingsvsnoels;

import com.example.davidamate.kingsvsnoels.framework.Game;
import com.example.davidamate.kingsvsnoels.framework.Graphics;
import com.example.davidamate.kingsvsnoels.framework.Pixmap;
import com.example.davidamate.kingsvsnoels.framework.Screen;
import com.example.davidamate.kingsvsnoels.framework.Input.TouchEvent;
import java.util.List;
/**
 * Created by david.amate on 11/01/2017.
 */
public class Gamescreen extends Screen{
    World w;

    enum GameState {
        Ready,
        Running,
        Paused,
        GameOver,
        Win
    }
    int xi = 0;
    int xf = 0;
    int yi = 0;
    int yf = 0;
    int cont=0;
    int distx = 0;
    int disty = 0;
    GameState state = GameState.Ready;
    public Gamescreen(Game game) {
        super(game);
        w=new World();
        if(Assets.intro.isPlaying()) {
            Assets.intro.stop();
        }

    }
    public void update(float deltaTime) {

        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        if(state == GameState.Ready)
            updateStarting(touchEvents);
        if(state == GameState.Running)
            updateRunning(touchEvents, deltaTime);
        if(state == GameState.Paused)
            updatePaused(touchEvents);
        if(state == GameState.GameOver)
            updateGameOver(touchEvents);
        if(state == GameState.Win)
            updateWin(touchEvents);
    }
    private void updateWin(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 70 && event.x <= 256) {
                    if(event.y > 300 && event.y <= 330) {
                        game.setScreen(new Gamescreen(game));
                        Assets.Victory.stop();
                    }

                }
                if(event.x > 100 && event.x <= 215) {
                    if(event.y > 350 && event.y <= 373) {
                        game.setScreen(new Gamescreen(game));
                        Assets.Victory.stop();
                    }

                }

            }
        }
    }
    private void updateStarting(List<TouchEvent> touchEvents) {
        if (Assets.gameOver.isPlaying())
        {
            Assets.gameOver.stop();
        }
        if (Assets. Victory.isPlaying())
        {
            Assets.Victory.stop();
        }
        if(cont==90) state = GameState.Running;
        else cont++;
    }
    private void getTouchInitial(TouchEvent event){
        xi = event.x;
        yi = event.y;
    }

    private void getTouchFinal(TouchEvent event){
        xf = event.x;
        yf = event.y;
        distx = xf-xi;
        disty = yf-yi;
    }
    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {

        if (Assets.Victory.isPlaying())
        {
            Assets.Victory.stop();
        }
        if (!Assets.gamemusic.isPlaying())
        {
            Assets.gamemusic.play();
        }
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_DOWN) {
                getTouchInitial(event);
            }

            if(event.type == TouchEvent.TOUCH_UP) {
                getTouchFinal(event);

                if(event.x < 64 && event.y < 64) {

                    state = GameState.Paused;
                    return;
                }
                //Slide
                if(w.cont<3) {
                    if (distx < 0 && Math.abs(disty) < Math.abs(distx)) {
                        w.Kings[w.cont].setDirection(King.LEFT);
                        w.Kings[w.cont].Movement();
                    } else if (distx > 0 && Math.abs(disty) < Math.abs(distx)) {
                        w.Kings[w.cont].setDirection(King.RIGHT);
                        w.Kings[w.cont].Movement();
                    }
                    if (disty < 0 && Math.abs(disty) > Math.abs(distx)) {
                        w.Kings[w.cont].setDirection(King.UP);
                        w.Kings[w.cont].Movement();
                    } else if (disty > 0 && Math.abs(disty) > Math.abs(distx)) {
                        w.Kings[w.cont].setDirection(King.DOWN);
                        w.Kings[w.cont].Movement();
                    }
                }

            }
        }

        w.update(deltaTime);

        if(w.gameOver) {

            state = GameState.GameOver;
        }
        if(w.Win) {


            state = GameState.Win;
        }

    }
    private void updatePaused(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 125) {

                        state = GameState.Running;
                        return;
                    }
                    if(event.y > 140 && event.y < 176) {
                        game.setScreen(new MainMenuScreen(game));
                        return;
                    }
                }
            }
        }
    }

    private void updateGameOver(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 70 && event.x <= 256) {
                    if(event.y > 300 && event.y <= 330) {
                        Assets.gameOver.stop();
                        game.setScreen(new Gamescreen(game));

                    }

                }
            }
        }
    }



    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.background, 0, 0);
        drawWorld();
        if(state == GameState.Ready)
            drawReadyUI();
        if(state == GameState.Running)
            drawRunningUI();
        if(state == GameState.Paused)
            drawPausedUI();
        if(state == GameState.GameOver)
            drawGameOverUI();
        if(state == GameState.Win)
            drawWinUI();
    }

    private void drawWinUI() {
        Graphics g = game.getGraphics();
        if(w.die==0) {
            g.drawPixmap(Assets.win3, 47, 100);
        }
        if(w.die==1) {
            g.drawPixmap(Assets.win2, 47, 100);
        }
        if(w.die==2) {
            g.drawPixmap(Assets.win1, 47, 100);
        }
        g.drawPixmap(Assets.RETRY, 70, 300);
        g.drawPixmap(Assets.exit, 100, 350);
        if (Assets.gamemusic.isPlaying())
        {
            Assets.gamemusic.stop();
        }
        if (!Assets.Victory.isPlaying()||Assets.Victory.isStopped())
        {
            Assets.Victory.play();
        }
        {
            Assets.Victory.play();
        }
    }

    private void drawReadyUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.ready, 47, 100);
    }
    private void drawRunningUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.buttons, 0, 0, 0, 128, 50, 50);
    }
    private void drawPausedUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.MenuPause, 80, 100);
    }
    private void drawGameOverUI() {
        Graphics g = game.getGraphics();
        if (Assets.gamemusic.isPlaying())
        {
            Assets.gamemusic.stop();
        }
        if (!Assets.gameOver.isPlaying())
        {
            Assets.gameOver.play();
        }
        g.drawPixmap(Assets.gameover, 70, 100);
        g.drawPixmap(Assets.RETRY, 70, 300);
    }

    private void drawWorld()
    {
        Graphics g=game.getGraphics();
        Noel[] noel=w.noel;
        int y=0;
        int x=0;
        Pixmap Pnoel=Assets.Noel;
        Pixmap King1=Assets.King1;
        Pixmap King2=Assets.King2;
        Pixmap King3=Assets.King3;
        Pixmap Pnoel2=Assets.Noel_girat;
        King[] king=w.Kings;
        for (int i=0;i<13;i++)
        {
            if(noel[i].direction==1)
            {
                x = noel[i].x * 32;
                y = noel[i].y * 32;

                g.drawPixmap(Pnoel2, x, y);
            }
            else {
                x = noel[i].x * 32;
                y = noel[i].y * 32;
                g.drawPixmap(Pnoel, x, y);
            }
        }
        if(!king[0].die) {
            if (king[0].type == 1) {

                x = king[0].x * 32;
                y = king[0].y * 32;
                g.drawPixmap(King1, x, y);
            }
        }
        if(!king[1].die) {
            if (king[1].type == 2) {

                x = king[1].x * 32;
                y = king[1].y * 32;
                g.drawPixmap(King2, x, y);
            }
        }
        if(!king[2].die) {
            if (king[2].type == 3) {

                x = king[2].x * 32;
                y = king[2].y * 32;
                g.drawPixmap(King3, x, y);
            }
        }
    }
    @Override
    public void pause() {
        if (Assets.gameOver.isPlaying())
        {
            Assets.gameOver.stop();
        }
        if (Assets. Victory.isPlaying())
        {
            Assets.Victory.stop();
        }
        if (Assets.gamemusic.isPlaying())
        {
            Assets.gamemusic.stop();
        }

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}

