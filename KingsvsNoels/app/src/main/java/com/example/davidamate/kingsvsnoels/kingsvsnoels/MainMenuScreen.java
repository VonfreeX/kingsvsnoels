package com.example.davidamate.kingsvsnoels.kingsvsnoels;

import com.example.davidamate.kingsvsnoels.framework.Game;
import com.example.davidamate.kingsvsnoels.framework.Graphics;
import com.example.davidamate.kingsvsnoels.framework.Pixmap;
import com.example.davidamate.kingsvsnoels.framework.Screen;
import com.example.davidamate.kingsvsnoels.framework.Input.TouchEvent;
import java.util.List;

/**
 * Created by david.amate on 21/12/2016.
 */
public class MainMenuScreen extends Screen {

    public MainMenuScreen(Game game) {
        super(game);
        Assets.intro.play();

        if (Assets.Victory.isPlaying())
        {
            Assets.Victory.stop();
        }
    }

    @Override
    public void update(float deltaTime) {


        Graphics g = game.getGraphics();
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {

                if(inBounds(event, 95, 300, 150, 36) ) {
                    game.setScreen(new Gamescreen(game));
                    //Assets.click.play(1);
                    return;
                }
                if(inBounds(event, 110, 380, 130, 36) ) {
                    game.setScreen(new Gamescreen(game));
                    System.exit(0);
                    //Assets.click.play(1);
                    return;
                }
           //

            }
        }
    }
    private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
        if(event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1)
            return true;
        else
            return false;
    }
    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.background, 0, 0);
        g.drawPixmap(Assets.GameTitle, 30, 20);
        g.drawPixmap(Assets.TrollNoel, 100, 300);
        g.drawPixmap(Assets.Tree, 0, 250);
        g.drawPixmap(Assets.Tree, 200, 250);
        g.drawPixmap(Assets.Play, 95, 300);
        g.drawPixmap(Assets.exit, 110, 380);


    }

    @Override
    public void pause() {
        Assets.intro.pause();
    }

    @Override
    public void resume() {
        Assets.intro.play();
    }

    @Override
    public void dispose() {

    }
}