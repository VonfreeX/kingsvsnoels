package com.example.davidamate.kingsvsnoels.kingsvsnoels;

import com.example.davidamate.kingsvsnoels.framework.Screen;
import com.example.davidamate.kingsvsnoels.framework.impl.AndroidGame;
import com.example.davidamate.kingsvsnoels.R;

/**
 * Created by david.amate on 21/12/2016.
 */
public class KingsvsNoels extends AndroidGame {
    @Override
    public Screen getStartScreen() {
        return new LoadingScreen(this);
    }

}
